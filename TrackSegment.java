import java.util.ArrayList;

public class TrackSegment {

	protected int length;
	protected int capacity;
	protected int currentCapacity;
	protected ArrayList<Train> currentTrains = new ArrayList<Train>();

	/**
	 * Accessor method to return the length of the track segment
	 * @return length - length of segment
	 */
	public int getLength() {
		return length;
	}

	/**
	 * Mutator method to set the length of the track segment
	 * @param len - length to set
	 */
	public void setLength(int len) {
		this.length = len;
	}

	/**
	 * Accessor method to return the max capacity of the segment
	 * @return capacity - the max number of trains the segment can hold
	 */
	public int getCapacity() {
		return capacity;
	}
	
	/**
	 * Mutator method to set the max capacity of the segment
	 * @param cap - the number to set the capacity of the segment
	 */
	public void setCapacity(int cap) {
		this.capacity = cap;
	}
	
	/**
	 * Accessor method to return the current capacity of the segment
	 * @return currCap - the current number of trains on the segment
	 */
	public int getCurrentCapacity() {
		int currCap = currentTrains.size();
		return currCap;
	}
	
	/**
	 * Accessor method to return the current trains on the segment
	 * @return currentTrains - arraylist holding the current trains on the segment
	 */
	public ArrayList<Train> getCurrentTrains(){
		return currentTrains;
	}
	
	/**
	 * Mutator method to set the current trains on the segment
	 * @param currTrains - arraylist to set the trains currently on the segment 
	 */
	public void setCurrentTrains(ArrayList<Train> currTrains) {
		this.currentTrains = currTrains;
	}

}	