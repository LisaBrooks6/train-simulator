public class Track extends TrackSegment{
	
	private int id;
	private final int MAX_CAPACITY = 1;

	/*
		The below constructors do the same thing. However for flexibility of the system,
		for example if the simulator was being extended to allow each track to hold more 
		than 1 train at a time then the first constuctor could be used. This would be done
		by deleting the if/else and replacing with this.capacity = cap;
	 */

	/**
	 * Constructor to allow the user to enter capacity
	 * @param id - int to set the id of the track
	 * @param length - int to set the length of the track
	 * @param cap - int to set the capacity of the track
	 */
	public Track(int id, int length, int cap){
		this.id = id;
		this.length = length;
		// if a capacity other than 1 is entered, this is altered
		if(cap != MAX_CAPACITY){
			this.capacity = MAX_CAPACITY;
		}
		else{
			this.capacity = cap;
		}
	}
	/**
	 * another constructor that allows the user to not have to enter a capacity
	 * @param id - int to set the id of the track
	 * @param length - int to set the length of the track
	 */
	public Track(int id, int length){
		this.id = id;
		this.length = length;
		this.capacity = MAX_CAPACITY;
	}

	/**
	 * Accessor to get the id of the track
	 * @return id - train id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Mutator to set the id of the track
	 * @param id - set the train id as this
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * This method returns a boolean to indicate if there is space on the track to add
	 * a train. If there is, this train is added to the arraylist currentTrains (this is instantiated in TrackSegment.java)
	 * @param t - train to be added
	 * @return true/false - boolean to say a train can be added or not
	 */
	public boolean addATrainToTrack(Train t) {
		// if there is space to add a train then increment current capacity and return true
		if(getCurrentCapacity() < getCapacity()){
			currentTrains.add(t);
			return true;
		}
		// if there is no space to add the train to the track, return false
		else{
			return false;
		}
	}

	/**
	 * This method returns a boolean to indicate if a train exists on a track.
	 * If there is, this train is removed from the arraylist currentTrains (this is instantiated in TrackSegment.java)
	 * @param t - train to be removed
	 * @return true/false - boolean to say a train can be removed or not
	 */
	public boolean removeTrainFromTrack(Train t) {
		// if there are > 0 trains on the track then decrement and return true
		if(getCurrentCapacity() > 0){
			currentTrains.remove(t);
			return true;
		}
		// if there are 0 trains on the track, do nothing and return false
		else{
			return false;
		}
	}
}