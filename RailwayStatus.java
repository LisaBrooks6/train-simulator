import java.lang.Math;
import java.util.Stack;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.Condition;
import java.util.Iterator;
import java.util.ArrayList;

public class RailwayStatus {
	
	private ArrayList<TrackSegment> routeAL;
	private ReentrantLock counterLock = new ReentrantLock();
	private Condition condition;

	/**
	 * Constructor for RailwayStatus
	 */
	public RailwayStatus(){
		routeAL = new ArrayList<TrackSegment>();
		try{
			condition = counterLock.newCondition();
		}
		catch(Exception e){}
	}

	/**
	 * This method adds a Station s to the arraylist routeAL
	 * @param s - Station object to be added
	 */
	public void addStation(Station s){
		routeAL.add(s);
	}

	/**
	 * This method adds a Track t to the arraylist routeAL
	 * @param t - Track object to be added
	 */
	public void addTrack(Track t){
		routeAL.add(t);
	}

	/** 
	 * Accessor for the current size of the arraylist
	 * @return routeSize - int representing the current size of the arraylist
	 */
	public int getRouteSize() {
		int routeSize = routeAL.size();
		return routeSize;
	}

	/**
	 * This method will check the status of the parameter segment, and if 
	 * there is space, the train will be added.
	 * @param t - Train object to be added
	 * @param routePos - TrackSegment the train is to be added to 
	 * @throws InterruptedException - thrown if there is a threading issue
	 */
	public void addTrain(Train t, TrackSegment routePos) throws InterruptedException {
		// first check which type of TrackSegment
		if(routePos instanceof Station) {
			Station station = (Station)routePos;
			if(station.addATrainToStation(t)==true){ // train has been able to be added to the station
				// set the position of the train
				t.setCurrentPos(station);
				// print the status
				printRailwayStatus();
			}
		}
		else if(routePos instanceof Track) {
			Track track = (Track)routePos;
			if(track.addATrainToTrack(t)==true){ // train has been able to be added to the track
				// set the position of the train
				t.setCurrentPos(track);
				// print the status 
				printRailwayStatus();
			}
		}
	}

	/**
	 * This method will send the train to the remove methods in Track/Station, which will subsequently check 
	 * if there is more than one train on/in the track/station. If so, this train will be removed.
	 * @param t - Train object to be removed
	 * @param routePos - TrackSegment the train has to be removed from
	 */
	public void removeTrain(Train t, TrackSegment routePos) {
		// first check which type of TrackSegment
		if(routePos instanceof Station) {
			Station station = (Station)routePos;
			station.removeTrainFromStation(t);
		}
		else if(routePos instanceof Track) {
			Track track = (Track)routePos;
			track.removeTrainFromTrack(t);
		}
	}

	/**
	 * This method will check the capacity on a particular segment, if there
	 * is space for a train to be moved to this segment a call is made to the 
	 * addTrain method.
	 * @param t - Train object to be moved
	 * @param segmentNo - TrackSegment that the train is attempting to move to
	 * @throws InterruptedException - thrown if something goes wrong with the conditions and locks
	 */
	public void moveTrain(Train t, int segmentNo) throws InterruptedException {
		TrackSegment segment = routeAL.get(segmentNo);
		long timeOnTrack;

		// lock the method
		counterLock.lock(); 
		int firstPosition = 0; // first element of arraylist routeAL
		int lastPosition = getRouteSize()-1; // last element of arraylist routeAL
		try{
			// while the train has not been able to move, let it wait until there is space
			while(true){
				// loop in this while loop until the train has been placed in the specified segment
				if(segment.getCurrentCapacity() < segment.getCapacity()){	
					// a train can be added as there is space on the track/ in the station
					if(segmentNo != firstPosition){
						// the segment is not first on the route, therefore get the index of segment in the arraylist 
						int index = routeAL.indexOf(segment);
						// remove the train from the segment before
						removeTrain(t, routeAL.get(index-1));
						// add the train to the specified track segment
						addTrain(t, segment);
					}
					else{
						/* the train will be added to the first segment on the route, therefore
						   there is no need to remove the train from a previous segment before adding
						 */
						addTrain(t, segment);
					}
					// the length of time a train should be on a specific segment of track
					timeOnTrack = segment.getLength()/t.getSpeed();
					// lock this train on the track segment for the specified time
					condition.awaitNanos(timeOnTrack);
					
					/* once the train object is asleep signal all other threads to continue.
					   This includes the threads holding trains which were told to wait as the
					   first station 'Glasgow' was at full capacity.  
					 */
					condition.signalAll();

					// if we have came to the end of the track and there is nowhere else a train should go
					if(segmentNo == lastPosition){	
						// remove the train from the end of the track
						removeTrain(t, segment);
					}
					return; // a train has been moved, therefore exit the loop
				}
				else{
					// if there is no space on the specified segment, wait until signalled
					condition.await();
				}
			}
		}
		finally{
			// unlock the method
			counterLock.unlock();
		}	
	}

	/**
	 * This method will print the current state of the railway at each call 
	 * This will be done by printing the content of the arraylist
	 * @throws InterruptedException - thrown if threading goes wrong
	 */
	public void printRailwayStatus() throws InterruptedException {
		// sleep so that the printing is not constant - there will be a 1 second delay
		Thread.sleep(1000);
		System.out.println();
		for(TrackSegment segment : routeAL){
			if(segment instanceof Station){
				/* if the segment is of type Station, print the name and get the trains 
				   currently in the station */
				Station s = (Station)segment;
				ArrayList<Train> currTrains = s.getCurrentTrains();
				String trainOnTrack = "";
				for(Train train : currTrains){
					trainOnTrack += train.getTrainId()+",";
				}
				System.out.print("|-"+s.getName()+"--"+trainOnTrack+"--|");
			}
			else if(segment instanceof Track){
				/* if the segment is of type Track, get the train currently 
				   on the segment of track and print */
				Track t = (Track)segment;
				ArrayList<Train> currTrains = t.getCurrentTrains();
				String trainOnTrack = "";
				for(Train train : currTrains){
					trainOnTrack += train.getTrainId();
				}
				System.out.print("|-track--"+trainOnTrack+"--|");
			}
		}
		System.out.println();
	}
}