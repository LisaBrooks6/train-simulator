public class LocalTrain extends Train {
	
	public LocalTrain(int id, int speed, RailwayStatus rs) {
		this.trainId = id;
		this.speed = speed;
		this.railwayStatus = rs;
	}
	
	/**
	 * This method tells the thread what to do when it has started
	 */
	public void run(){
		// run method for local train
		try{
			for(int i = 0; i < railwayStatus.getRouteSize(); i++){
				// i would be the segment we want to move the train to
				railwayStatus.moveTrain(this, i);
			}
		}
		catch(Exception e){}
	}
}