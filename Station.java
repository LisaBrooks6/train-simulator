import java.util.ArrayList;
public class Station extends TrackSegment{
	
	private String name;

	/**
	 * Station constructor
	 * @param name - String to set the name of the station
	 * @param length - int to set the length of the station
	 * @param capacity - int to set the capacity of the station
	 */
	public Station(String name, int length, int capacity) {
		this.name = name;
		this.length = length;
		this.capacity = capacity;
	}

	/**
	 * Accessor method to return the name of the station
	 * @return name - the name of the train
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Mutator method to set the name of the station
	 * @param name - set the name of the train to this
	 */	
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * This method returns a boolean to indicate if there is space in the station to add
	 * a train. If there is, this train is added to the arraylist currentTrains (this is instantiated in TrackSegment.java)
	 * @param t - train to be added
	 * @return true/false - boolean to say a train can be added or not
	 */
	public boolean addATrainToStation(Train t) {
		// if there is space to add a train then increment current capacity and return true
		if(getCurrentCapacity() < getCapacity()){
			currentTrains.add(t);
			return true;
		}
		// if there is no space to add the train to the track, return false
		else{
			return false;
		}
	}
	
	/**
	 * This method returns a boolean to indicate if a train exists in a station.
	 * If there is, this train is removed from the arraylist currentTrains (this is instantiated in TrackSegment.java)
	 * @param t - train to be removed
	 * @return true/false - boolean to say a train can be removed or not
	 */
	public boolean removeTrainFromStation(Train t) {
		// if there are > 0 trains on the track then decrement and return true
		if(getCurrentCapacity() > 0){
			currentTrains.remove(t);
			return true;
		}
		// if there are 0 trains on the track, do nothing and return false
		else{
			return false;
		}
	}
} 