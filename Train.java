public abstract class Train extends Thread implements Runnable {
	
	protected int speed;
	protected int trainId;
	protected RailwayStatus railwayStatus; // shared object
	protected TrackSegment currentPos;

	/**
	 * Accessor to return the speed of the train 
	 * @return speed - int to represent the speed of the train 
	 */
	public int getSpeed() {
		return speed;
	}
	/**
	 * Accessor to return the train id
	 * @return trainId - int to represent the id of the train 
	 */
	public int getTrainId() {
		return trainId;
	}
	
	/**
	 * Accessor to return the status of the railway
	 * @return railwayStatus - the shared object
	 */
	public RailwayStatus getRailwayStatus() {
		return railwayStatus;
	}
	
	/**
	 * Accessor to return the current track segment 
	 * @return currentPos - the current segment of the track 
	 */
	public TrackSegment getCurrentPos() {
		return currentPos;
	}

	/**
	 * Mutator to set the train speed
	 * @param speed - int to set the speed of the train
	 */
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	/**
	 * Mutator to set the train id
	 * @param id - int to set the train id
	 */
	public void setTrainId(int id) {
		this.trainId = id;
	}
	
	/**
	 * Mutator to set the railway status
	 * @param rs - RailwayStatus to set the shared object 
	 */
	public void setRailwayStatus(RailwayStatus rs) {
		this.railwayStatus = rs;
	}
	
	/**
	 * Mutator to set the current position on the track
	 * @param segment - TrackSegment to set the current pos on the route  
	 */
	public void setCurrentPos(TrackSegment segment) {
		this.currentPos = segment;
	}
	
	/**
	 * Abstract method which will be implemented in classes which extend Train
	 */
	public abstract void run();
}