import java.util.Stack;
import java.lang.Math;
import java.util.ArrayList;

public class Tester {

	// the following instance variables are just for testing to get ranges for random lengths and capacities
	private static final int MAX_TRACK_LEN = 1000;
	private static final int MIN_TRACK_LEN = 800;
	private static final int MAX_CAP = 5; 
	private static final int MIN_CAP = 1;
	private static final int NO_OF_TRACKS = 5;
	private static final int MIN_SLOW_SPEED = 10;
	private static final int MAX_SLOW_SPEED = 30;
	private static final int MIN_FAST_SPEED = 50;
	private static final int MAX_FAST_SPEED = 70;
	

	public static void main(String[] args) throws InterruptedException, OutOfMemoryError {
		RailwayStatus railway = new RailwayStatus();
		// build a sample route
		// add a station
		railway.addStation(new Station("Glasgow", getRandomTrackLength(), getRandomCapacity()));
		// add 5 tracks 
		for(int i = 0; i < NO_OF_TRACKS; i++){
			railway.addTrack(new Track(i, getRandomTrackLength(), getRandomCapacity()));
		}
		// add a station
		railway.addStation(new Station("Polmont", getRandomTrackLength(), getRandomCapacity()));
		// add another 5 tracks
		for(int i = NO_OF_TRACKS; i < 2*NO_OF_TRACKS; i++){
			railway.addTrack(new Track(i, getRandomTrackLength(), getRandomCapacity()));
		}
		// add a final station
		railway.addStation(new Station("Edinburgh", getRandomTrackLength(), getRandomCapacity()));
		// print the route
		// railway.printRailwayStatus();
		System.out.println("size: "+railway.getRouteSize());
		
		// create an array of train objects
		Train [] trains = new Train[10];
		trains[0] = new LocalTrain(1, 2, railway);
		trains[1] = new ExpressTrain(2, 10, railway);
		trains[2] = new LocalTrain(3, 3, railway);
		trains[3] = new ExpressTrain(4, 12, railway);
		trains[4] = new LocalTrain(5, 3, railway);
		trains[5] = new ExpressTrain(6, 20, railway);
		trains[6] = new ExpressTrain(7, 18, railway);
		trains[7] = new LocalTrain(8, 2, railway);
		trains[8] = new ExpressTrain(9, 22, railway);
		trains[9] = new LocalTrain(10, 4, railway);

		// ArrayList<Train> trains = new ArrayList<Train>();
		// // initial train 
		// trains.add(new ExpressTrain(0, getRandomFastSpeed(), railway));
		// int i = 1;
		// while(true){
		// 	if(trains.get(trains.size()-1) instanceof ExpressTrain){
		// 		// add a local train with a random slow speed to the arraylist
		// 		trains.add(new LocalTrain(i, getRandomSlowSpeed(), railway));
		// 		// System.out.println("EXPRESS TRAIN CREATED WITH i = "+i);

		// 	}
		// 	else{
		// 		// add an express train with a random fast speed to the arraylist
		// 		trains.add(new ExpressTrain(i, getRandomFastSpeed(), railway));
		// 		// System.out.println("LOCAL TRAIN CREATED WITH i = "+i);
		// 	}
			
		// 	// make this element a thread and start it
		// 	Thread thread = new Thread(trains.get(trains.size()-1));
		// 	thread.start();

		// 	// increment i so that no trains have the same id
		// 	i++;
		// }


		try{
			// create an array of Thread objects
			Thread [] thread = new Thread[trains.length];

			// populate each thread object
			for(int i = 0; i < trains.length; i++){
				thread[i] = new Thread(trains[i]);
				// start thread
				thread[i].start();
				// railway.printRailwayStatus();
				Thread.sleep(1000);	
			}
		}
		catch(Exception e) {}

		// create an object which prints out the status of the railway every 10 seconds
		

	}

	public static int getRandomTrackLength(){
		return (int)(Math.random()*(MAX_TRACK_LEN - MIN_TRACK_LEN)+MIN_TRACK_LEN);
	}
	public static int getRandomCapacity(){
		return (int)(Math.random()*(MAX_CAP - MIN_CAP)+MIN_CAP);
	}
	public static int getRandomSlowSpeed(){
		return (int)(Math.random()*(MAX_SLOW_SPEED - MIN_SLOW_SPEED)+MIN_SLOW_SPEED);
	}
	public static int getRandomFastSpeed(){
		return (int)(Math.random()*(MAX_FAST_SPEED - MIN_FAST_SPEED)+MIN_FAST_SPEED);
	}
}