import java.util.Stack;
import java.lang.Math;
import java.util.ArrayList;

public class RunMe {
	// the following instance variables are just for testing to get ranges for random lengths and capacities
	private static final int MAX_TRACK_LEN = 1000;
	private static final int MIN_TRACK_LEN = 800;
	private static final int MAX_CAP = 5; 
	private static final int MIN_CAP = 1;
	private static final int NO_OF_TRACKS = 5;
	private static final int MIN_SLOW_SPEED = 10;
	private static final int MAX_SLOW_SPEED = 30;
	private static final int MIN_FAST_SPEED = 50;
	private static final int MAX_FAST_SPEED = 70;
	private static final int MAX_RAND_DELAY = 2000;
	private static final int MIN_RAND_DELAY = 1000;
	private static final int RAND_MAX = 5;
	private static final int RAND_MIN = 1;

	public static void main(String[] args) throws InterruptedException {
		RailwayStatus railway = new RailwayStatus();
		// build a sample route
		// add a station
		railway.addStation(new Station("Glasgow", getRandomTrackLength(), getRandomCapacity()));
		// add 5 tracks 
		for(int i = 0; i < NO_OF_TRACKS; i++){
			railway.addTrack(new Track(i, getRandomTrackLength(), getRandomCapacity()));
		}
		// add a station
		railway.addStation(new Station("Polmont", getRandomTrackLength(), getRandomCapacity()));
		// add another 5 tracks
		for(int i = NO_OF_TRACKS; i < 2*NO_OF_TRACKS; i++){
			railway.addTrack(new Track(i, getRandomTrackLength()));
		}
		// add a final station
		railway.addStation(new Station("Edinburgh", getRandomTrackLength(), getRandomCapacity()));

		int i = 0;
		ArrayList<Thread> threads = new ArrayList<Thread>();
		ArrayList<Train> trains = new ArrayList<Train>();
		/* less than 1000 so that there is no OutOfMemoryError occurring. This was occurring due to 
		   too many threads being created such that the JVM couldn't handle them.
		 */
		while(true){//} && threads.size()<1000){
			// get a random int that is between 1 and 5	
			int rand = (int)(Math.random()*(RAND_MAX-RAND_MIN)+RAND_MIN);
			// random delay between 1 second and 2 seconds
			int randomDelay = (int)(Math.random()*(MAX_RAND_DELAY-MIN_RAND_DELAY)+MIN_RAND_DELAY);
			// for creation of random trains, say if rand is less than or equal to 2 then a local train is created
			if(rand<=2){
				// add a local train with a random slow speed to the arraylist
				trains.add(new LocalTrain(i, getRandomSlowSpeed(), railway));
			}
			else{
				// add an express train with a random fast speed to the arraylist
				trains.add(new ExpressTrain(i, getRandomFastSpeed(), railway));
			}
			// make this element a thread and start it
			threads.add(new Thread(trains.get(trains.size()-1)));
			threads.get(i).start();		
			// threads.get(i).sleep(randomDelay);
			Thread.sleep(randomDelay);
			// increment i so that no trains have the same id
			i++;
		}
	}

	public static int getRandomTrackLength(){
		return (int)(Math.random()*(MAX_TRACK_LEN - MIN_TRACK_LEN)+MIN_TRACK_LEN);
	}
	public static int getRandomCapacity(){
		return (int)(Math.random()*(MAX_CAP - MIN_CAP)+MIN_CAP);
	}
	public static int getRandomSlowSpeed(){
		return (int)(Math.random()*(MAX_SLOW_SPEED - MIN_SLOW_SPEED)+MIN_SLOW_SPEED);
	}
	public static int getRandomFastSpeed(){
		return (int)(Math.random()*(MAX_FAST_SPEED - MIN_FAST_SPEED)+MIN_FAST_SPEED);
	}
}
