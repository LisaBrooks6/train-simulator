# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is an assessed exercise for an Advanced Programming course which uses threads to place trains on a track at different times. This is a command line program.
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Clone this repository 
* Type the following commands in the terminal (MAC): 
* javac *.java
* java RunMe

### Example ###
* The program will illustrate in the following manner: |-Glasgow--2-|-track--|-track-1-|-Edinburgh--| where Glasgow and Edinburgh are two stations, train 2 is currently in Glasgow and train 1 is on the second track. 
* The RunMe.java class creates Stations and Tracks and adds new Trains to the track at different intervals of time. All tracks can hold at most 1 train at a time, stations have varying capacities.

### Who do I talk to? ###
* Lisa Brooks